public class Aluno{

	private String nomeAluno;
	private String matriculaAluno;
	
	public void setNomeAluno(String umNomeAluno){
		nomeAluno = umNomeAluno;
	}
	public String getNomeAluno(){
		return nomeAluno;
	}
	public void setMatriculaAluno(String umaMatricula){
		matriculaAluno = umaMatricula;
	}
	public String getMatriculaAluno(){
		return matriculaAluno;
	}

}
